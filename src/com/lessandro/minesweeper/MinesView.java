package com.lessandro.minesweeper;

import java.util.Random;

import android.app.NotificationManager;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.Resources;
import android.graphics.*;
import android.graphics.Paint.Style;
import android.graphics.drawable.Drawable;
import android.os.DeadObjectException;
import android.view.MotionEvent;
import android.view.View;

public class MinesView extends View {
	
	private final int size = 24;
	private final int tilenum = 12;
	private final int top = 24;
	private int sw, sh, w, h, offx, offy, curx, cury;
	private int[][] grid;
	private int[][] vgrid;
	private Bitmap[] tiles;
	private int turn, mines, minesleft, bluescore, redscore, mycolor;
	
	private static final int WATER = 9;
	private static final int MINE = 10;
	static final int RED = 10;
	static final int BLUE = 11;

	private static final Random RNG = new Random();
	private int seed;
	
	static final String ACTION = "com.lessandro.minesweeper.PLAY";
	private Minesweeper minesweeper;

	// graphic init
	
	public MinesView(Context context) {
		super(context);

		tiles = new Bitmap[tilenum];

        Resources r = context.getResources();
        setTile(0, r.getDrawable(R.drawable.concrete));
        for (int i=1; i<9; i++) {
        	setNumTile(i, r.getDrawable(R.drawable.concrete));
        }
        setTile(WATER, r.getDrawable(R.drawable.water));
        setTile(10, r.getDrawable(R.drawable.red));
        setTile(11, r.getDrawable(R.drawable.blue));
	}

	private void setTile(int key, Drawable tile) {
        Bitmap bitmap = Bitmap.createBitmap(size, size, true);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, size, size);
        tile.draw(canvas);
        tiles[key] = bitmap;
	}

	private void setNumTile(int num, Drawable tile) {
        Bitmap bitmap = Bitmap.createBitmap(size, size, true);
        Canvas canvas = new Canvas(bitmap);
        tile.setBounds(0, 0, size, size);
        tile.draw(canvas);
        Paint paint = new Paint();
        paint.setAntiAlias(true);
        paint.setTextSize(20);
        canvas.drawText(""+num, 6, 20, paint);
        tiles[num] = bitmap;
	}

	@Override
    public void onDraw(Canvas canvas) {
		super.onDraw(canvas);

		drawGrid(canvas);
		drawTop(canvas);
	}

	private void drawGrid(Canvas canvas) {
		Paint paint = new Paint();
		paint.setColor(Color.BLACK);
    	canvas.drawRect(0, 0, sw, sh, paint);

    	for (int j=0; j<h; j++) {
    		for (int i=0; i<w; i++) {
    			int x = offx+i*size;
    			int y = offy+j*size;
    			canvas.drawBitmap(tiles[vgrid[j][i]], x, y, paint);
    			if (i == curx && j == cury) {
    				Paint stroke = new Paint();
    				stroke.setStyle(Style.STROKE);
    				stroke.setColor(mycolor == RED ? Color.RED : Color.BLUE);
    				canvas.drawRect(x, y, x+size-1, y+size-1, stroke);
    			}
    		}
    	}
	}

	private void drawTop(Canvas canvas) {
		Paint paint = new Paint();

    	paint.setAntiAlias(true);
    	paint.setTextSize(16);
    	paint.setColor(Color.WHITE);

    	canvas.drawText("Turn:", 4, 20, paint);
    	canvas.drawBitmap(tiles[turn], 50, 0, paint);
    	canvas.drawText("Red: " + redscore, 95, 20, paint);
    	canvas.drawText("Blue: " + bluescore, 165, 20, paint);
    	canvas.drawText("Mines: " + minesleft, 240, 20, paint);
	}

	// game logic init

	public void setData(Minesweeper minesweeper, int color, int seed) {
    	this.minesweeper = minesweeper;
    	this.mycolor = color;

    	this.seed = (seed == 0) ? RNG.nextInt() : seed;
    	RNG.setSeed(this.seed);
    }

    @Override
    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
    	sw = w;
    	sh = h;
    	this.w = w / size;
    	this.h = (h - top) / size;
    	offx = (w - this.w*size)/2;
    	offy = top + (h - top - this.h*size)/2;
    	
    	grid = new int[this.h][this.w];
    	vgrid = new int[this.h][this.w];

    	init();
    }

    public void init() {
    	mines = (w*h/5) | 1;
    	turn = RED;
    	redscore = 0;
    	bluescore = 0;
    	minesleft = mines;
    	
    	clearGrid();
    	putGridMines();
    	putGridNumbers();

    	invalidate();
    }

    private void clearGrid() {
    	for (int j=0; j<h; j++) {
    		for (int i=0; i<w; i++) {
    			vgrid[j][i] = WATER;
    			grid[j][i] = 0;
     		}
    	}
	}

	private void putGridMines() {
    	for (int i=0; i<mines; i++) {
    		int x, y;

    		do {
    			x = RNG.nextInt(w);
    			y = RNG.nextInt(h);
    		} while (grid[y][x] == MINE);

    		grid[y][x] = MINE;
    	}
    }
    
    private void putGridNumbers() {
    	for (int j=0; j<h; j++) {
    		for (int i=0; i<w; i++) {
    			if (grid[j][i] == MINE) continue;

    			int num = 0;
    			for (int k=0; k<9; k++) {
    				num += isMine((i-1)+(k/3), (j-1)+(k%3)) ? 1 : 0;
    			}
    			grid[j][i] = num;
    		}
    	}
    }

    // communication
    
	void sendStart() {
        Intent intent = new Intent(ACTION);
        intent.putExtra("action", "start-ok");
        intent.putExtra("seed", ""+seed);
        sendIntent(intent);
	}
	
    private void sendClick(int x, int y) {
        Intent intent = new Intent(ACTION);
        intent.putExtra("action", "click");
        intent.putExtra("x", ""+x);
        intent.putExtra("y", ""+y);
        sendIntent(intent);
    }
    
    void sendIntent(Intent intent) {
        try {
			minesweeper.mXmppSession.sendDataMessage(minesweeper.enemy, intent);
		} catch (DeadObjectException e) {
			showMessage("Error sending message");
		}
    }

    private void showMessage(CharSequence msg) {
        NotificationManager nm = (NotificationManager)mContext.getSystemService(
                Context.NOTIFICATION_SERVICE);
        nm.notifyWithText(42, msg,  NotificationManager.LENGTH_LONG, null);
    }

    // clicks and game logic

	@Override
    public boolean onMotionEvent(MotionEvent event) {
        int action = event.getAction();

    	int x = (int)(event.getX()-offx) / size;
    	int y = (int)(event.getY()-offy) / size;

        if (action == MotionEvent.ACTION_UP) {

        	if (turn != mycolor) return false;
        	if (isOutside(x, y)) return false;
    		if (vgrid[y][x] != WATER) return false;

   			doClick(x, y);
   			sendClick(x, y);
        }

        if (action == MotionEvent.ACTION_MOVE || action == MotionEvent.ACTION_DOWN) {
        	if (curx != x || cury != y) {
        		curx = x;
        		cury = y;
        		invalidate();
        	}
        }

        return true;
    }

	void doClick(int x, int y) {
		if (isMine(x, y)) {
			if (turn == RED) {
				redscore++;
			} else {
				bluescore++;
			}
			minesleft--;
		}
		
  		if (click(x, y)) {
  			turn = (turn == RED) ? BLUE : RED;
  		}
  		invalidate();

  		doWin();
	}

	private void doWin() {
  		int win = 0;
  		if (redscore > mines/2) win = RED;
  		if (bluescore > mines/2) win = BLUE;
  		
  		if (win != 0) {
  			String msg = (win == mycolor) ? "Victory." : "Hahahah. Loser.";
  			
  			DialogInterface.OnClickListener finisher = new DialogInterface.OnClickListener() {
				public void onClick(DialogInterface dialog, int whichButton) {
					minesweeper.finish();
				}
  			};

  			minesweeper.showAlert("Minesweeper", msg, "Ok", finisher, false, null);
  		}
	}

	private boolean click(int x, int y) {
		if (!isValid(x, y)) return false;

		vgrid[y][x] = grid[y][x]; 

		if (grid[y][x] == MINE) {
			vgrid[y][x] = turn;
			return false;
		}

		if (grid[y][x] == 0) {
			for (int k=0; k<9; k++) {
				click((x-1)+(k/3), (y-1)+(k%3));
			}
		}

		return true;
	}

    private boolean isMine(int x, int y) {
    	if (!isValid(x, y)) return false;
    	return grid[y][x] == MINE;
    }

    private boolean isValid(int x, int y) {
    	if (isOutside(x, y)) return false;
    	if (vgrid[y][x] != WATER) return false;
    	return true;
    }

	private boolean isOutside(int x, int y) {
 		return (x < 0 || y < 0 || x >= w || y >= h);
	}
}
