package com.lessandro.minesweeper;

import com.google.android.xmppService.IXmppService;
import com.google.android.xmppService.IXmppSession;

import android.app.Activity;
import android.app.NotificationManager;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.IntentReceiver;
import android.content.ServiceConnection;
import android.os.Bundle;
import android.os.DeadObjectException;
import android.os.IBinder;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Minesweeper extends Activity {

	IXmppSession mXmppSession = null;
	String enemy;

	private MinesView mMinesView;
	private TextView mStatus;
	private EditText mEnemy;
	private Button mStart;

    @Override
    public void onCreate(Bundle icicle) {
        super.onCreate(icicle);

        mMinesView = new MinesView(this);
        
        setContentView(R.layout.minesweeper);
        mEnemy = (EditText)findViewById(R.id.enemy);
        mStatus = (TextView)findViewById(R.id.status);
        mStart = (Button)findViewById(R.id.start); 
        mStart.setOnClickListener(mOnClickListener);
        mStart.setEnabled(false);

        bindService((new Intent()).setComponent(
                com.google.android.xmppService.XmppConstants.XMPP_SERVICE_COMPONENT),
                null, mConnection, 0);
    }

	IntentReceiver mIntentReceiver = new IntentReceiver() {
        public void onReceiveIntent(Context context, Intent intent) {
            handleIntent(intent);
        }
    };

    @Override
    public void onResume() {
    	super.onResume();
    	IntentFilter filter = new IntentFilter();
        filter.addAction(MinesView.ACTION);
        registerReceiver(mIntentReceiver, filter);
    }
    
    @Override
    public void onPause() {
    	super.onPause();
    	unregisterReceiver(mIntentReceiver);
    }
    
    private void showMessage(CharSequence msg) {
        NotificationManager nm = (NotificationManager)getSystemService(
                Context.NOTIFICATION_SERVICE);
        nm.notifyWithText(42, msg,  NotificationManager.LENGTH_LONG, null);
    }

    private void handleIntent(Intent intent) {
    	
        Bundle bundle = intent.getExtras();
        if (bundle == null) return;

        String action = bundle.getString("action"); 
        if (action == null) return;
        
        if (action.equals("start")) {
        	enemy = bundle.getString("me");
        	showMessage("Playing with "+enemy+"\nYou are BLUE");
        	mMinesView.setData(this, MinesView.BLUE, 0);
        	mMinesView.sendStart();
        	setContentView(mMinesView);
        }

        if (action.equals("start-ok")) {
        	showMessage("Playing with "+enemy+"\nYou are RED");
        	int seed = Integer.valueOf(bundle.getString("seed"));
        	mMinesView.setData(this, MinesView.RED, seed);
        	setContentView(mMinesView);
        }

        if (action.equals("click")) {
        	int x = Integer.valueOf(bundle.getString("x"));
        	int y = Integer.valueOf(bundle.getString("y"));
        	mMinesView.doClick(x, y);
        }
    }

    private void sendAction(String action) {
        try {
            Intent intent = new Intent(MinesView.ACTION);
            intent.putExtra("action", action);
			intent.putExtra("me", mXmppSession.getUsername());

			mXmppSession.sendDataMessage(enemy, intent);
		} catch (DeadObjectException e) {
			showMessage("Error sending message");
			mStart.setEnabled(false);
		}
    }

    private ServiceConnection mConnection = new ServiceConnection() {
        public void onServiceConnected(ComponentName className, IBinder service) {
            IXmppService xmppService = IXmppService.Stub.asInterface(service);

            try {
            	mXmppSession = xmppService.getDefaultSession();
            }
            catch (DeadObjectException ex) { }

            if (mXmppSession == null) {
                showMessage("Error getting Xmpp Session");
            } else {
            	mStart.setEnabled(true);
            	mStatus.setText("XmppSession up. Waiting for intents.");
            }
        }

        public void onServiceDisconnected(ComponentName className) {
            mXmppSession = null;
            mStart.setEnabled(false);
        }
    };

    private View.OnClickListener mOnClickListener = new View.OnClickListener() {
        public void onClick(View v) {
        	enemy = mEnemy.getText().toString();
        	sendAction("start");
        }
    };
}
